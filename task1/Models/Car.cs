﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task1.Models
{
    public class Car
    {
       public int Id { get; set; }
       public string ModelName { get; set; }
       public string MakeName { get; set; }
       public string VinCode { get; set; }
       public DateTime Year { get; set; }
       public int UserID { get; set; }
       public User User { get; set; }
    }
}
