﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task1.Models
{
    public class CarContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }
        public CarContext(DbContextOptions<CarContext> options)
            : base(options)
        {
        }
    }
}
